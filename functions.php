<?php

function script_me_up() {
	wp_enqueue_script('uniform', get_bloginfo('template_url').'/_inc/js/uniform/jquery.uniform.js', 'jquery');	
	wp_enqueue_script('functions', get_bloginfo('template_url').'/_inc/js/functions.js', 'jquery');	
	wp_enqueue_style('uniform-css', get_bloginfo('template_url').'/_inc/js/uniform/uniform.default.css');	
}

add_action('wp_enqueue_scripts', 'script_me_up');
add_theme_support( 'post-thumbnails' );

//Disable images linking to their attachment page by default
update_option('image_default_link_type','none');

//Remove gallery inline styling
add_filter( 'use_default_gallery_style', '__return_false' );

//Force link to file instead of attachment in galleries
remove_shortcode('gallery', 'gallery_shortcode');
add_shortcode( 'gallery', 'my_gallery_shortcode' );
function my_gallery_shortcode( $atts )
{
    $atts['link'] = 'file';
    return gallery_shortcode( $atts );
}

//Change the_excerpt length and remove the trailing ellipsis
function custom_excerpt_length( $length ) {
	return 15;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
function new_excerpt_more($more) {
	return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');

 //Image Sizes
if ( function_exists( 'add_image_size' ) ) { 
    add_image_size( 'homebanner', 900, 465, true );
} 

//Menu
add_theme_support( 'menus' );

function register_menus() {
  register_nav_menus(
    array(
      'header-menu' => __( 'Header Menu' )  
    )
  );
}
add_action( 'init', 'register_menus' );

//Disable images linking to their Attachment page when inserted via the Media Picker
update_option('image_default_link_type','none');

//Clean up wp_head
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'parent_post_rel_link');
remove_action('wp_head', 'start_post_rel_link');
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head');

add_filter("gform_currencies", "update_currency");

function update_currency($currencies) {
    $currencies['AUD'] = array(
		"name" => __("Australian Dollar", "gravityforms"), 
		"symbol_left" => '$', 
		"symbol_right" => '', 
		"symbol_padding" => '', 
		"thousand_separator" => ',', 
		"decimal_separator" => '.', 
		"decimals" => 2);
    return $currencies; 
}

add_filter("gform_name_first", "change_name_first", 10, 2);

function change_name_first($label, $form_id){
    return "First name";
}

add_filter("gform_name_last", "change_name_last", 10, 2);

function change_name_last($label, $form_id){
    return "Family name";
}

if( function_exists('acf_add_options_page') ) {

    acf_add_options_page();

}

?>