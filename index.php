<?php get_header(); ?>

<div id="content">

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<article>
		<h2><?php the_title(); ?></h2>
		<time datetime="<?php echo date(DATE_W3C); ?>" class="updated"><?php the_time('d M Y') ?></time>
		<?php the_content(); ?>
	</article>

	<?php endwhile; endif; ?>
	
	<div id="pagination">
		<div class="next-posts"><?php next_posts_link('&laquo; Older Entries') ?></div>
		<div class="prev-posts"><?php previous_posts_link('Newer Entries &raquo;') ?></div>
	</div>

</div>

<?php get_footer(); ?>