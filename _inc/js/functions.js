jQuery(document).ready(function($) {

	$("a.fancybox").fancybox({
		'transitionIn'	:	'elastic',
		'transitionOut'	:	'elastic',
		'padding'	:	0,
		'hideOnContentClick' : true, 
		'titleShow'	: false, 
		'opacity'	:	true
	});
	
	$('a[rel="external"]').click( function() {
		window.open( $(this).attr('href') );
		return false;
	});
	
	
	    //gallery lightbox
    $('.gallery').magnificPopup({
        delegate: 'a',
        type: 'image',
        gallery: {
            enabled: true
        },
        zoom: {
            enabled: true
        },
        image: {
            titleSrc: function(item) {
                return item.el.parents('.gallery-item').find('.wp-caption-text').html() || '';
            }
        }
    });

     //back to top
    $(window).scroll(function () {
        
        var scrollTop = $(window).scrollTop();
        var top_button = $('a.back-to-top');
        
        if (scrollTop >= 300 && !top_button.is(':visible')) {
            top_button.show();            
        } else {
            if(scrollTop < 300 && top_button.is(':visible')) {  
                top_button.hide();
            }
        } 
    });
    $("a.back-to-top").click(function() {
        $("html, body").animate({ scrollTop: 0 }, 300);
        return false;
    });
	
	 //mobile menu
    $('#main-nav li.menu-item-has-children.current-menu-item, #main-nav li.menu-item-has-children.current-menu-ancestor').each(function(){
        $(this).addClass('expanded');     
    });
    $('#main-nav li.menu-item-has-children').click(function(){
        if($('.mobile-menu').is(':visible')){
            var li = $(this);
            var child_menu = li.children('ul');
            if(child_menu.is(':visible')){
                li.removeClass('expanded').children('ul').slideUp(200);
            } else {
                li.addClass('expanded').children('ul').slideDown(200);
            }
            return false;
        }
    });
    $('#main-nav li.menu-item-has-children a').click(function(event){
        event.stopPropagation();    
    });    
    $('.mobile-menu').click( function() {
        $('nav#main-nav ul.menu').slideToggle('fast');        
        $(this).toggleClass('expanded');        
        return false;
    });
    
    //owl carousel
    $('.home-banner').owlCarousel({
        slideSpeed : 300,
        navigation: false,
        pagination: true,
        singleItem:true,
        transitionStyle : "fade",
		autoPlay : 6000
    });
	
	//Newsletter subscribe form
	$('#signup_form').submit(function() {
		var formaction = $("#signup_form").attr('action');
		if ($("#yourname").val() != "" && $("#youremail").val() != "") {
			$.fancybox({ 
				'padding'		: 50,
				'centerOnScroll'	: true,
				'overlayOpacity'	: 0.3,
				'overlayColor'		: '#111',
				'opacity'		: true,
				'autoScale'		: false,
				'transitionIn'		: 'elastic',
				'transitionOut'		: 'elastic',
				'href'		: $(this).attr('action')+'?name='+$("#yourname").val()+'&email='+$("#youremail").val(), 
				'type'			: 'ajax'
			});
		} else {
			alert('Please enter a valid name and email.');
		}
		return false;
	});
	
	//When inserting images via the WP Media Picker, users sometimes inadvertently leave the link src
	//so the inserted image links to an Attachment page. This is usually unnecessary and confusing, so
	//let's make those Attachment links simply popup the image in a Fancybox.
	$('a[rel*="attachment"]').click(function(){
		var thelink = $(this).children('img').attr('src');
		$.fancybox({ 
		'padding'		: 0,
		'centerOnScroll'	: true,
		'overlayOpacity'	: 0.3,
		'overlayColor'		: '#111',
		'opacity'		: true,
		'autoScale'		: false,
		'transitionIn'		: 'elastic',
		'transitionOut'		: 'elastic',
		'href'		: thelink
		});
		return false;
	});
    
    checkLocationField();
    function checkLocationField(){
        var is_local = ( $('.location-radios').find('input[value="1"]:checked').size() > 0 );
        if(!is_local){
            $('#Location').show();   
        } else {
            $('#Location').val('').hide();     
        }
    }
    
    $('.location-radios').find('input:radio').change(function(){
        checkLocationField();
    });
    
    $('form.buynow-form').submit(function(){
        $(this).find('.errormsg').remove();
        
        if($(this).find('select[name="product_option1"]').size() > 0){
           if($(this).find('select[name="product_option1"]').val() == ''){
               $(this).append('<div class="errormsg">Please select an option</div>');               
               return false;
           }    
       } 
       
       return true; 
    });
	
});

window.onload = function() {
	//jQuery fix for browsers that don't support the HTML5 'placeholder' property
	jQuery('input, textarea').placeholder();
}