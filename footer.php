	<div class="footer-menu-wrap">
		<div style="padding-bottom:20px;text-align:center; width:100%;">
			<div class="sub-menu-wrap sub-menu-color">
				<ul id="sub-menu">
					<?php wp_nav_menu(array('container'=>'','theme_location'=>'header-menu', 'depth' => 1)); ?>
				</ul>
			</div>
		</div>
		
		<div class="bx-fb-2 footer-menu-box">		
			<?php if( have_rows('sponsors', 'options') ): ?>
				<?php while ( have_rows('sponsors', 'options') ) : the_row(); ?>
					<div class="footer-sponsors circler">
						<a href="<?php the_sub_field('sponsor_logo_link', 'options'); ?>">
						<img src="<?php the_sub_field('sponsor_logo', 'options'); ?>"></img>
						</a>
					</div>
				<?php endwhile; ?>
				<?php else : ?>
			<?php endif; ?>
		</div>
	</div>
	<div>&nbsp; </div>
	<div class="footer-organisers">
		<?php the_field('footer_organisers', 'options'); ?> 
	</div>
	<div class="footer-florish">
	<a href="http://www.cleverstarfish.com" id="sitebystarfish" target="blank">
		"Site by Clever Starfish"
		::after
	</a>
	</div>

</footer>

</body>
</html>

