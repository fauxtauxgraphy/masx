<?php get_header(); ?>

<div class="bx-fb-1 page-banner" style="background-image: url(<?php the_field('page_banner_img'); ?>); background-size: cover; background-position:top;">
	<div class="color-overlay-pos color-overlay"></div>
	<div class="page-banner-title ani-fi-500">
		<h1><?php echo get_the_title($ID); ?></h1>
	</div>

	<div class="sub-menu-wrap sub-menu-bgcolor">
		<div class="wave-1"></div>
		<ul id="sub-menu">
			<li>
				<a href="<?php echo get_permalink( $post->post_parent ); ?>" >
				<?php echo get_the_title( $post->post_parent ); ?>
				</a>
			</li>
			<?php
				if ($post->post_parent) {
					$page = $post->post_parent;
				} else {
					$page = $post->ID;
				}

				$children = wp_list_pages(array(
					'child_of' => $page,
					'echo' => '0',
					'title_li' => ''
				));

				if ($children) {
					echo "\n".$children."\n";
				} 
			?>
		</ul>
	</div>
</div>
	
<!--<div class="bx-fb-1 page-subhead">
	<span class="ani-fi-750"><?php the_field('page_subheading'); ?></span>
</div> -->
<div class="wave-2"></div>
<div class="page-content-wrap">
	<div class="content">
		<div class="page-content ani-fi-1500">
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<?php the_content(); ?>
			<?php endwhile; endif; ?>
			<?php if( get_field('page_cta_title') ): ?>
			<div class="page-cta">
				<div class="page-cta-leader">
					<?php the_field('page_cta_leader'); ?>
				</div>
				<div class="button">
					<a href="<?php the_field('page_cta_link'); ?>"><?php the_field('page_cta_title'); ?></a>
				</div>
			</div>
			<?php endif; ?>
		</div>
	</div>
</div>

<div class="wave-1"></div>
<footer>

	<a href="<?php the_field('footer_announcement_link', 'options'); ?>"><div class="bx-fl-1 footer-announcement"><?php the_field('announcement', 'options'); ?>
	</div></a>
	<!--<img src="<?php echo get_template_directory_uri(); ?>/_inc/img/florish_right.png" /> --></div></a>
	
<?php get_footer(); ?>

