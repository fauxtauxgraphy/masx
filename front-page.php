<?php get_header(); ?>

<div class="bx-fb-1 home-banner" style="background-image: url(<?php the_field('banner_hero'); ?>); background-size: cover; background-position:top;">
	<div class="color-overlay-pos color-overlay"></div>
	<img class="ani-logo-in" src="<?php the_field('banner_logo'); ?>" alt="<?php the_field('banner_logo_alt_text'); ?>" />
</div>

<div class="wave-3"></div>
<div class="bx-fb-1">
	<div class="home-headline-wrap">
		<div class="home-headline">
			<div class="home-headline-img"><a href="https://goo.gl/maps/kPjA8QsKzDN2" target="_blank"><img src="<?php the_field('map_pin_image'); ?>"/></a></div>
			<div class="ani-fi-2000"><?php the_field('home_headline'); ?></div>
			<div class="home-byline ani-fi-2000"><?php the_field('home_byline'); ?></div>
		</div>
	</div>
</div>
<div class="bx-fb-1">
	<div class="home-quicklink-button-bar">
		<a href="<?php the_field('quicklink_1_link'); ?>"><div class="home-quicklink-button"><?php the_field('quicklink_1_text'); ?></div></a> 
		<a href="<?php the_field('quicklink_2_link'); ?>"><div class="home-quicklink-button"><?php the_field('quicklink_2_text'); ?></div></a> 
		<a href="<?php the_field('quicklink_3_link'); ?>"><div class="home-quicklink-button"><?php the_field('quicklink_3_text'); ?></div></a>
	</div>
</div>

<div class="bx-fb-1">
	<div class="home-welcome ani-fi-3000">
		<div class="circler home-welcome-image">
			<img src="<?php the_field('event_welcome_image'); ?>" />
		</div>
		<div class="home-welcome-text">
			<?php the_field('event_welcome'); ?>
		</div>
	</div>
</div>
<div class="home-content-wrapper" >
	<div class="content">
		<div class="home-content">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<?php the_content(); ?>	
			<?php endwhile; else: endif; ?>
		</div>	
	</div>
</div>

<div class="wave-1"></div>
<div class="home-blog">
	<div class="home-blog-section">
		<h2>Latest News</h2>
	</div>	
	<div class="bx-fb-3 home-blog-listing">
				<?php
		//Latest blog entry
		query_posts('post_type=post&posts_per_page=10');
		if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<div class="post">
				<div class="home-blog-img circler circler-1">
					<a href="<?php the_permalink() ?>"><img class="home-blog-img-bg" src="<?php the_field('page_banner_img'); ?>" /></a>
				</div>
				<div class="home-blog-title">
					<div class="meta">
						<span class="date"><?php the_time('D, jS M Y') ?></span>
					</div>
					<a href="<?php the_permalink() ?>" title="<?php the_title(); ?>"><?php the_title('<h3>', '</h3>'); ?></a>
				</div>	
			</div>
		<?php endwhile; else:
		endif;	
		wp_reset_query(); ?>
	</div>
</div>

<footer>
	<div class="wave-1"></div>
	<a href="<?php the_field('footer_announcement_link', 'options'); ?>"><div class="bx-fl-1 footer-announcement"><?php the_field('announcement', 'options'); ?>
	</div></a>
	<br>
	
<?php get_footer(); ?>

