/*
 * Template Name: Registration
 * Description: Rego Page
 */
 
<?php get_header(); ?>

<div class="bx-fb-1 page-banner" style="background-image: url(<?php the_field('page_banner_img'); ?>);">
	<div class="page-banner-title ani-fi-500">
		<h1><?php echo get_the_title($ID); ?></h1>
	</div>
</div>
	
<div class="bx-fb-1 page-subhead">
	<span class="ani-fi-750"><?php the_field('page_subheading'); ?></span>
</div>

<div class="page-content-wrap">
	<div class="content">
		<div class="page-content ani-fi-1500">
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<?php the_content(); ?>
			<?php endwhile; endif; ?>
		</div>
	</div>
</div>

<footer>

<?php get_footer(); ?>


