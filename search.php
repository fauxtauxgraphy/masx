<?php get_header(); ?>

<div id="content">

	<?php
	$query = query_posts($query_string."&post_type=page&posts_per_page=-1");
	$numresults = $wp_query->post_count;
	if ($numresults == 0) { $numresults = "no"; }
	if ($numresults == 1) { $plural = ''; } else { $plural = 's'; }
	echo "<h1>Search Results</h1>";
	echo "<h2>Your search for '".$_GET['s']."' returned ".$numresults." result".$plural.".</h2>";
	while (have_posts()) : the_post();
		echo the_title('<h3 style="'.$thestyles.'"><a href="/index.php?p='.$post->ID.'">','</a></h3>','true');
		the_excerpt();
	endwhile;
	if (!have_posts()) {
		echo "<h3>Sorry, your search returned no results.</h3>";
	}
	?>

</div>

<?php get_footer(); ?>