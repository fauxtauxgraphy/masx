<?php get_header(); ?>

<div class="bx-fb-1 page-banner" style="background-image: url(<?php the_field('page_banner_img'); ?>);">
	<div class="color-overlay-pos color-overlay"></div>
	<div class="page-banner-title ani-fi-500">
		<h1><?php echo get_the_title($ID); ?></h1>
	</div>
</div>


<div class="wave-1"></div>
<div class="page-content-wrap">
	<div class="content">
		<div class="page-content ani-fi-1500">
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<?php the_content(); ?>
			<?php endwhile; endif; ?>
				<div class="blog-nav">
					<div class="button button_left"><?php previous_post('%title'); ?></div> <div class="button"><?php next_post('%title'); ?></div>
				</div>
		</div>
	</div>
</div>	

<div class="wave-3"></div>
<footer>

	<a href="<?php the_field('footer_announcement_link', 'options'); ?>"><div class="bx-fl-1 footer-announcement"><?php the_field('announcement', 'options'); ?>
	</div></a>
	<!--<img src="<?php echo get_template_directory_uri(); ?>/_inc/img/florish_right.png" /> --></div></a>
	
<?php get_footer(); ?>

